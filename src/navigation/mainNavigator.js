import React, { useState, useEffect } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Splash from '../screens/Splash';
import Home from '../screens/Home';
import Profile from '../screens/Profile';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="Profile" component={Profile} />
    </Drawer.Navigator>
  )
}

const ApplicationNavigator = () => {
  const [performShow, setPerformShow] = useState(true);

  useEffect(() => {
    setTimeout(() => setPerformShow(false), 2000);
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        {performShow && <Stack.Screen name="Splash" component={Splash} />}
        <Stack.Screen name="DrawerNavigator" component={DrawerNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default ApplicationNavigator;