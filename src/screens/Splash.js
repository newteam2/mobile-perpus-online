import React from 'react';
import { SafeAreaView, Text, StyleSheet } from 'react-native';

export default Splash = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Text>Splash</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#eaeaea"
  }
})

