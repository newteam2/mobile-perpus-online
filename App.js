/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import ApplicationNavigator from './src/navigation/mainNavigator';

const App: () => React$Node = () => {
  return (
    <ApplicationNavigator />
  );
};

export default App;
